# Changelog


## [qemu-0.1.0], [vbox-0.1.0] - 2020-11-8
### Added
- Service chrony for time synchronization over NTP
- This CHANGELOG file and versioning


[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali-2019.4/-/tree/qemu-0.1.0
[vbox-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/kali-2019.4/-/tree/vbox-0.1.0
