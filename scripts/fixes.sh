#!/bin/bash -x

# update kali apt key
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive wget -q -O - https://archive.kali.org/archive-key.asc | apt-key add
DEBIAN_FRONTEND=noninteractive apt-get update

# set GRUB_TIMEOUT to 0
sudo sed -i "s/^GRUB_TIMEOUT=[0-9]*/GRUB_TIMEOUT=0/" /etc/default/grub
sudo update-grub

# disable ssh root login using password
sed -i 's/#\?PermitRootLogin.*/PermitRootLogin without-password/g' /etc/ssh/sshd_config

# install resolvconf
apt-get -y install resolvconf

# install and configure chrony
apt-get -y install chrony
sed -i 's/pool .*/pool 0.pool.ntp.org iburst/' /etc/chrony/chrony.conf
systemctl enable chrony
systemctl start chrony
